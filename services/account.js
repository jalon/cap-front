import { http } from './config';

export default{

    showBalance:(id) => {
        return http.get('balance/'+id); 
    },

    showUser:(id) => {
        return http.get('user/'+id); 
    },

    setTransation:(transation) => {
        return http.post('/trasations', transation);
    }
}